import { Component, OnInit, Input } from '@angular/core';
import { FaceLike } from '../models/face-like';
import { FaceLikeService } from '../services/face-like.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent {
  @Input() image!: FaceLike;

  ngOnInit(){
    
  }

  constructor(private faceLikeService:FaceLikeService, private router : Router){

  }
  onViewFaceLike(){
    this.router.navigateByUrl(`facelikes/${this.image.id}`);
  }
}
