import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FaceLikeListComponent } from "./face-like-list/face-like-list.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { SingleFaceLikeComponent } from "./single-face-like/single-face-like.component";

const routes: Routes = [
    { path: 'facelikes/:id', component: SingleFaceLikeComponent },
    { path: 'facelikes', component: FaceLikeListComponent },
    { path: '', component: LandingPageComponent }    
];

@NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [
      RouterModule
    ]
  })

export class AppRoutingModule{

}