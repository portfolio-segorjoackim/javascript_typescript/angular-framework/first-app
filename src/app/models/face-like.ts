import { FaceLikeService } from "../services/face-like.service"

export class FaceLike {
    id!:number
    name!:string
    describe!:string
    likes = 0
    like = false
    location!:string
    picture!:string
    

    constructor(_id:number,_name:string,_describe:string,_location:string,_picture:string,_likes?:any){
        this.id = _id
        this.name = _name
        this.describe = _describe
        this.location = _location
        this.picture = _picture
        if(_likes > 0){
            this.likes += _likes
        }
        
    }

 
}
