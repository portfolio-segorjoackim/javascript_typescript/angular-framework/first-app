import { Injectable } from "@angular/core";
import { FaceLike } from "../models/face-like";

@Injectable({
    providedIn :"root"
})

export class FaceLikeService {

    faceLikes: FaceLike[] = [
        new FaceLike(
            1,
            "Tour Eiffel",
            "Une magnifique tour !",
            "Paris",
            "../../assets/img/images (4).jfif",
            250
        ),
      new FaceLike(
        2,
        "Tour Eiffel",
        "Une magnifique tour !",
        "Paris",
        "../../assets/img/images (2).jfif",
      ),
      new FaceLike(
        3,
        "Casino",
        "Woah un bon packtol!",
        "Las Vegas",
        "../../assets/img/images (5).jfif",
      )
    ];

    getAllFaceLike(){
        return this.faceLikes
    }
    
    getFaceLikeById(_id:number) {
        const theFaceLike = this.faceLikes.find(theFaceLike => theFaceLike.id === _id);
        if(!theFaceLike){
            throw new Error('Cette id ne correspond à aucun FaceLike !');
        }else{
            return theFaceLike;
        }
    }


    changeLikesFaceLikeById(_id:number, _option:"Likes"|"Dislike"){
        const faceLike = this.getFaceLikeById(_id);
        _option === 'Likes' ? faceLike.likes++ : faceLike.likes--;
    }

}