import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})

export class LandingPageComponent {

  ngOnInit(){

  }
  constructor( private router : Router){

  }
  onContinue() {
    this.router.navigateByUrl('facelikes');
  }
}
