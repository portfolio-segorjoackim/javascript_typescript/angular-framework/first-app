import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaceLikeListComponent } from './face-like-list.component';

describe('FaceLikeListComponent', () => {
  let component: FaceLikeListComponent;
  let fixture: ComponentFixture<FaceLikeListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FaceLikeListComponent]
    });
    fixture = TestBed.createComponent(FaceLikeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
