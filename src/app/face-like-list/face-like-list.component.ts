import { Component, OnInit } from '@angular/core';
import { FaceLike } from '../models/face-like';
import { FaceLikeService } from '../services/face-like.service';

@Component({
  selector: 'app-face-like-list',
  templateUrl: './face-like-list.component.html',
  styleUrls: ['./face-like-list.component.scss']
})
export class FaceLikeListComponent {

  faceLikes!: FaceLike[];

  constructor(private faceLikeService: FaceLikeService){

  }

  ngOnInit(){
    this.faceLikes = this.faceLikeService.getAllFaceLike()
  }
  
}
