import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleFaceLikeComponent } from './single-face-like.component';

describe('SingleFaceLikeComponent', () => {
  let component: SingleFaceLikeComponent;
  let fixture: ComponentFixture<SingleFaceLikeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SingleFaceLikeComponent]
    });
    fixture = TestBed.createComponent(SingleFaceLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
