import { Component, Input } from '@angular/core';
import { FaceLike } from '../models/face-like';
import { FaceLikeService } from '../services/face-like.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-face-like',
  templateUrl: './single-face-like.component.html',
  styleUrls: ['./single-face-like.component.scss']
})
export class SingleFaceLikeComponent {

  btnText!:string;
  @Input() image!: FaceLike;

  ngOnInit(){
    const faceId = +this.route.snapshot.params["id"];
    this.image = this.faceLikeService.getFaceLikeById(faceId);
    this.image.like === false ? this.btnText = "Likes" :this.btnText ="Dislike";
  }

  constructor(private faceLikeService:FaceLikeService, private route : ActivatedRoute){

  }

  addLike(){
    if (this.image.like === false){
        this.faceLikeService.changeLikesFaceLikeById(this.image.id,"Likes");
        this.btnText ="Dislike";
        this.image.like = true;
    }
    else{
        this.faceLikeService.changeLikesFaceLikeById(this.image.id,"Dislike");
        this.btnText ="Likes";
        this.image.like = false;
    }
  }
}
